export class User{
    name: string;
    surname: string;
    picture: string;
    loggedIn: boolean;
    accessLevel: number;
}


