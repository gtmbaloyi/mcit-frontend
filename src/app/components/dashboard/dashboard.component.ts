import { Component, OnInit, ViewChild } from '@angular/core';
import {DashboardService} from "../../services/dashboard.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    pieChart = [];

    constructor( private dashboardService: DashboardService) {
    }

    ngOnInit() {
        this.pieChart = this.dashboardService.pieChart();

    }

}